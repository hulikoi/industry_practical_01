package birds;

/**
 * This class defines a sparrow. A sparrow may fly or swim. It is not a native New Zealand bird.
 *
 * @author Write your UPI here
 */
public class LittlePenguin extends Bird {

    public LittlePenguin(IFly flyBehaviour, ISwim swimBehaviour) {
        super(flyBehaviour, swimBehaviour);
        this.name = "Little Penguin";
    }

    // TODO step c.i. modify this method
    @Override
    public String greet(String name) {
        return "Honk honk " + name;
    }

    @Override
    public boolean isNative() {
        return true;
    }

}
