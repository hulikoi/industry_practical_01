package birds;

/**
 * This class defines a pukeko. A sparrow may fly or swim. It is a native New Zealand bird.
 * TODO step d. modify this class appropriately
 *
 * @author Write your UPI here
 */
public class Pukeko extends Bird implements WetlandBird {
    // TODO step d.i. create a private String field called habitat
    private String habitat;

    // TODO step d.ii. modify this constructor
    public Pukeko(String habitat, IFly flyBehaviour, ISwim swimBehaviour) {
        super(flyBehaviour, swimBehaviour);
        this.name = "Pukeko";
        this.habitat = habitat;
    }

    // TODO step d.iii. create a new constructor that only takes a String parameter
    public Pukeko(String habitat) {
        super(new NoFlying(), new Swimming());//
//        this.flyBehaviour = new NoFlying();
//        this.swimBehaviour = new Swimming();
        this.name = "Pukeko";
        this.habitat = habitat;
    }


    // TODO step d.iv. modify this method
    @Override
    public String greet(String name) {
        return "Peki peki " + name;
    }

    // TODO step d.v. modfiy this method
    @Override
    public boolean isNative() {
        return true;
    }

    @Override
    public String favouritePlaces() {
        return habitat;
    }
}
