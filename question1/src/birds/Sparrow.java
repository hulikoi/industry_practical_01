package birds;

/**
 * This class defines a sparrow. A sparrow may fly or swim. It is not a native New Zealand bird.
 *
 * @author Write your UPI here
 */
public class Sparrow extends Bird {

    public Sparrow(IFly flyBehaviour, ISwim swimBehaviour) {
        super(flyBehaviour, swimBehaviour);
        this.name = "Sparrow";
    }

    // TODO step c.i. modify this method
    @Override
    public String greet(String name) {
        return "Chirp chirp " + this.name;
    }

    @Override
    public boolean isNative() {
        return false;
    }

}
