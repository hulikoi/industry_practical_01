package birds;

/**
 * This class defines a bird. A bird  may fly or swim.
 * Do not modify this class!
 *
 * @author Yu-Cheng Tu
 */
public abstract class Bird {
    // Flying behaviour of the bird
    protected IFly flyBehaviour;

    // Swimming behaviour of the bird
    protected ISwim swimBehaviour;

    // Size of the bird
    protected double size;

    // Name of the bird
    protected String name;

    /**
     * Constructs a bird with a random size between 1.0 and 50.0 (excluding 50.0).
     * The name of the bird is "Bird"
     *
     * @param flyBehaviour  Flying behaviour of the bird
     * @param swimBehaviour Swimming behaviour of the bird
     */
    public Bird(IFly flyBehaviour, ISwim swimBehaviour) {
        this.flyBehaviour = flyBehaviour;
        this.swimBehaviour = swimBehaviour;
        this.size = Math.random() * 50 + 1;
        this.name = "Bird";
    }

    /**
     * The bird does the greeting. For example, Tweet tweet George.
     *
     * @param name the name of the person for the greeting
     * @return Greetings from the bird
     */
    public abstract String greet(String name);

    /**
     * Is the bird native to New Zealand?
     *
     * @return true if the bird is a native New Zealand bird
     */
    public abstract boolean isNative();

    /**
     * The bird tries to fly
     *
     * @return A statement of what the bird is doing
     */
    public String doFly() {
        return flyBehaviour.fly();
    }

    /**
     * The bird tries to swim
     *
     * @return A statement of what the bird is doing
     */
    public String doSwim() {
        return swimBehaviour.swim();
    }

    /**
     * Determines whether this bird is bigger than the other bird
     *
     * @param other The other bird for comparison
     * @return true if this bird's size is larger than the other's
     */
    public boolean isBiggerThan(Bird other) {
        return this.size > other.size;
    }

    /**
     * @return String representation of the bird
     */
    @Override
    public String toString() {
        return String.format("%s, a %s New Zealand bird, with a random size of %.2f.",
                this.name, isNative() ? "native" : "non-native", this.size);
    }

}
