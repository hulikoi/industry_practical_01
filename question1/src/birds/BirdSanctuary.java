package birds;

/**
 * This class generates different birds and displays the information to the console.
 * TODO step f. modify this class appropriately
 *
 * @author Write your UPI here
 */
public class BirdSanctuary {
    // An array of habitats for the wetland birds
    private final String[] HABITATS = {"swamps", "streams", "lagoons", "parks", "streams", "paddocks", "croplands", "parks", "swamps", "lagoons"};

    // This is the main method. Do not change this!
    public static void main(String[] args) {
        new BirdSanctuary().start();
    }

    // This is the start method. Do not change this!
    private void start() {
        System.out.println("Welcome to Auckland Bird Sanctuary. Here are our birds.");
        System.out.println("==========================");

        // TODO step f.i. create the method createBirds which returns an array of 10 different Bird objects
        Bird[] nzBirds = createBirds();

        // TODO step f.ii. create the method printAllBirds
        printAllBirds(nzBirds);

        System.out.println("==========================");
        System.out.println("Our wetland birds like to say hi!");
        System.out.println("-----------------------");

        // TODO step f.iii. create the method printWetlandBirds
        printWetlandBirds(nzBirds);

        System.out.println("==========================");
        System.out.println("Our biggest bird likes to say hi too!");
        System.out.println("-----------------------");

        // TODO step f.iv. create the method printBiggestBird
        printBiggestBird(nzBirds);

        System.out.println("==========================");
        System.out.println("Thank you for visiting us :)");
    }

    //find out who our biggest bird is and fat shame them
    private void printBiggestBird(Bird[] nzBirds) {
        Bird bigBirdy = nzBirds[0];
        for (int i = 1; i < nzBirds.length; i++) {
            if (bigBirdy.size < nzBirds[i].size) {
                bigBirdy = nzBirds[i];
            }
        }

        System.out.printf("%s. I am the biggest bird with a random size of %.2f.\n", bigBirdy.greet("Cameron"), bigBirdy.size);
    }

    //print a greeting from our native birds
    private void printWetlandBirds(Bird[] nzBirds) {
        for (int i = 0; i < nzBirds.length; i++) {
            Bird wetBirdy = nzBirds[i];
            if (wetBirdy instanceof WetlandBird) {
                System.out.println(wetBirdy.greet("Cameron.") + " My favourite places are " + ((WetlandBird) wetBirdy).favouritePlaces() + ".");
            }
        }
    }

    // Just the facts ma'am about the birds in our collection.
    private void printAllBirds(Bird[] nzBirds) {
        for (int i = 0; i < nzBirds.length; i++) {
            System.out.println(nzBirds[i].toString());
        }
    }

    //Make 10 birds in our lab
    private Bird[] createBirds() {
        //create a bird array
        Bird[] birds = new Bird[10];


        for (int i = 0; i < birds.length; i++) {
            //randomly choose a new habitat for pukekos
            String habitat = HABITATS[(int) (Math.random() * 10)];
            //generate a random num 0-2 to choose bird type
            int birdType = (int) (Math.random() * 100) % 3;

            if (birdType == 0) {
                birds[i] = new LittlePenguin(new NoFlying(), new Swimming());
            } else if (birdType == 1) {
                birds[i] = new Pukeko(habitat);
            } else if (birdType == 2) {
                birds[i] = new Sparrow(new Flying(), new Drowning());
            }
        }

        return birds;
    }

}
