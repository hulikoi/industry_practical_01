package birds;

public class NoFlying implements IFly {
    @Override
    public String fly() {
        return "I can't fly :(";
    }
}
