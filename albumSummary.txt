311,  "311", 1995
2 Live Crew,  "As Nasty As They Wanna Be", 1989
21st Century Schizoid Band,  "Pictures of a City – Live in New York", 2006
3 Doors Down,  "The Better Life", 2000
8 Foot Sativa,  "Hate Made Me", 2002
A Flock Of Seagulls,  "A Flock Of Seagulls", 1982
A Perfect Circle,  "Mer de Noms", 2000
A Perfect Circle,  "Thirteenth Step", 2003
A Perfect Circle,  "eMOTIVe", 2004
A Tribe Called Quest,  "The Anthology", 1999
A.F.I.,  "Sing The Sorrow", 2003
A.F.I.,  "decemberunderground", 2006
A.F.I.,  "Crash Love", 2009
AC/DC,  "Back In Black", 1980
Aerosmith,  "Big Ones", 1994
Afroman,  "The Good Times", 2000
a-ha,  "Headlines and Deadlines: The Hits of a-ha", 1991
Alice Cooper,  "Trash", 1989
Alice Cooper,  "Hey Stoopid", 1991
Alice Cooper,  "The Last Temptation", 1994
Alice in Chains,  "The Essential Alice in Chains", 2006
Alien Ant Farm,  "ANThology", 1999
Alien Ant Farm,  "truANT", 2003
Alphaville,  "Alphaville: The Singles Collection", 1988
American Head Charge,  "The War Of Art", 2001
Andrew W.K.,  "I Get Wet", 2001
Arch Enemy,  "Doomsday Machine", 2005
Arrested Development,  "3 Years, 5 Months & 2 Days in the Life Of...", 1992
Art Garfunkel,  "Garfunkel", 1988
ASG,  "Win Us Over", 2007
Ash,  "1977", 1996
Asia,  "Asia", 1982
Audioslave,  "Audioslave", 2002
Audioslave,  "Out of Exile", 2005
Audioslave,  "Revelations", 2006
Avenged Sevenfold,  "City of Evil", 2005
Bad Religion,  "Suffer", 1988
Bad Religion,  "No Control", 1989
Bad Religion,  "Against the Grain", 1991
Bad Religion,  "Generator", 1992
Bad Religion,  "Recipe For Hate", 1993
Bad Religion,  "Stranger Than Fiction", 1994
Bad Religion,  "The Gray Race", 1996
Bad Religion,  "No Substance", 1998
Bad Religion,  "The New America", 2000
Bad Religion,  "The Process Of Belief", 2002
Bad Religion,  "The Empire Strikes First", 2004
Bad Religion,  "New Maps of Hell", 2007
Bad Religion,  "The Dissent of Man", 2010
Bad Religion,  "True North", 2013
Badfinger,  "Timeless....The Musical Legacy", 2013
Beastie Boys,  "Beastie Boys Anthology: The Sounds of Science", 1999
Beastie Boys,  "To the 5 Boroughs", 2004
Beck,  "Odelay", 1996
Better Than Ezra,  "Deluxe", 1995
Billy Idol,  "Billy Idol", 1982
Billy Joel,  "The Essential Billy Joel", 2001
Biz Markie,  "The Biz Never Sleeps", 1989
Blind Melon,  "Blind Melon", 1992
Blink-182,  "Dude Ranch", 1997
Blink-182,  "Enema of the State", 1999
Blink-182,  "Take Off Your Pants and Jacket", 2001
Blink-182,  "blink‐182", 2003
Blink-182,  "Neighborhoods", 2011
Blondie,  "Greatest Hits", 2002
Blue Öyster Cult,  "Super Hits", 2004
Bob Dylan,  "The Essential Bob Dylan", 2001
Bruce Springsteen,  "Greetings from Asbury Park, N.J.", 1973
Bruce Springsteen,  "Born To Run", 1975
Bruce Springsteen,  "The River", 1980
Bruce Springsteen,  "Nebraska", 1982
Bruce Springsteen,  "Born In The U.S.A.", 1984
Bruce Springsteen,  "Greatest Hits", 1995
Bruce Springsteen,  "The Ghost of Tom Joad", 1995
Bush,  "Sixteen Stone", 1994
Carpenters,  "Gold: Greatest Hits", 2000
Chuck Berry,  "Icon: Chuck Berry", 2011
Coldplay,  "Viva La Vida or Death and All His Friends", 2008
Creedence Clearwater Revival,  "Best Of", 2008
Culture Club,  "Colour By Numbers", 1983
Cypress Hill,  "Skull & Bones", 2000
Cypress Hill,  "Till Death Do Us Part", 2004
Cypress Hill,  "Greatest Hits from the Bong", 2005
Danzig,  "Danzig", 1988
Dave Brubeck,  "The Essential Dave Brubeck", 2003
David Bowie,  "Changesbowie", 1990
Deadsy,  "Commencement", 1999
Def Leppard,  "Pyromania", 1983
Def Leppard,  "Hysteria", 1987
Def Leppard,  "Adrenalize", 1992
Deftones,  "Adrenaline", 1995
Deftones,  "Around the Fur", 1997
Deftones,  "White Pony", 2000
Deftones,  "Deftones", 2003
Deftones,  "Saturday Night Wrist", 2006
Deftones,  "Diamond Eyes", 2010
Depeche Mode,  "Violator", 1990
Die Antwoord,  "$O$", 2009
Die Antwoord,  "Ten$ion", 2012
Die Antwoord,  "Donker Mag", 2014
Dinosaur Jr.,  "Dinosaur", 1985
Dinosaur Jr.,  "You’re Living All Over Me", 1987
Dinosaur Jr.,  "Bug", 1988
Dinosaur Jr.,  "Green Mind", 1991
Dinosaur Jr.,  "Where You Been", 1993
Dinosaur Jr.,  "Without a Sound", 1994
Dinosaur Jr.,  "Hand It Over", 1997
Dinosaur Jr.,  "Ear Bleeding Country: The Best of Dinosaur Jr.", 2001
Dinosaur Jr.,  "Beyond", 2007
Dinosaur Jr.,  "Farm", 2009
Dinosaur Jr.,  "I Bet on Sky", 2012
Dio,  "Holy Diver", 1983
Disturbed,  "The Sickness", 2000
Disturbed,  "Believe", 2002
Disturbed,  "Ten Thousand Fists", 2005
Disturbed,  "Indestructible", 2008
Donovan,  "Donovan's Greatest Hits", 1968
DragonForce,  "Inhuman Rampage", 2005
Dwight Twilley Band,  "Twilley Don't Mind", 1977
Eagles,  "Selected Works 1972–1999", 2013
Echo & The Bunnymen,  "More Songs To Learn And Sing", 2006
Eddy Grant,  "Road To Reparation: The Very Best Of Eddy Grant", 2008
Enigma,  "Love Sensuality Devotion: The Greatest Hits", 2001
Eric Clapton,  "Complete Clapton", 1983
Escape Club,  "Wild, Wild, West", 1988
Eurythmics,  "Sweet Dreams (Are Made Of This)", 1982
Eurythmics,  "Touch", 1983
Everclear,  "World of Noise", 1993
Everclear,  "Sparkle and Fade", 1995
Everclear,  "So Much for the Afterglow", 1997
Everclear,  "Songs from an American Movie, Vol. 1: Learning How to Smile", 2000
Everclear,  "Songs from an American Movie, Vol. 2: Good Time for a Bad Attitude", 2000
Faith No More,  "The Real Thing", 1989
Faith No More,  "Angel Dust", 1992
Faith No More,  "King for a Day… Fool for a Lifetime", 1995
Faith No More,  "Album of the Year", 1997
Faith No More,  "Who Cares a Lot? The Greatest Hits", 1998
Fleetwood Mac,  "Greatest Hits", 1988
Foo Fighters,  "Foo Fighters", 1995
Foo Fighters,  "The Colour and the Shape", 1997
Foo Fighters,  "There Is Nothing Left to Lose", 1999
Foo Fighters,  "One by One", 2002
Foo Fighters,  "In Your Honor", 2005
Foreigner,  "Agent Provocateur", 1984
Foster The People,  "Torches", 2011
Fugees,  "The Score", 1996
Gerry Rafferty,  "The Best of Gerry Rafferty", 2003
Goldfinger,  "Hang-Ups", 1997
Green Day,  "1,039/Smoothed Out Slappy Hours", 1991
Green Day,  "Kerplunk", 1992
Green Day,  "Dookie", 1994
Green Day,  "Insomniac", 1995
Green Day,  "Nimrod", 1997
Green Day,  "Warning", 2000
Green Day,  "American Idiot", 2004
Green Day,  "21st Century Breakdown", 2009
Green Day,  "¡Uno!", 2012
Green Day,  "¡Dos!", 2012
Green Day,  "¡Tré!", 2012
Green Jelly,  "Cereal Killer Soundtrack", 1993
Guns N' Roses,  "Apetite For Destruction", 1987
Guns N' Roses,  "G N' R Lies", 1988
Guns N' Roses,  "Use Your Illusion I", 1991
Guns N' Roses,  "Use Your Illusion II", 1991
Guns N' Roses,  "Chinese Democracy", 2008
Hall & Oates,  "Big Bam Boom", 1984
Harry Nilsson,  "Aerial Ballet", 1968
Helmet,  "Unsung: The Best of Helmet (1991-1997)", 2004
Hole,  "Live Through This", 1994
Hole,  "Celebrity Skin", 1998
Hoobastank,  "Hoobastank", 2001
Hoobastank,  "The Reason", 2004
Hootie & the Blowfish,  "Cracked Rear View", 1994
Iggy and The Stooges,  "Raw Power", 1973
Iggy Azalea,  "The New Classic", 2014
Iggy Pop,  "Lust for Life", 1977
Iggy Pop,  "The Idiot", 1977
Immortal Technique,  "Revolutionary, Vol. 1", 2001
In Flames,  "The Jester Race", 1995
In Flames,  "Black Ash Inheritance", 1997
In Flames,  "Whoracle", 1997
In Flames,  "Colony", 1999
In Flames,  "Reroute to Remain", 2002
In Flames,  "Soundtrack to Your Escape", 2004
In Flames,  "Come Clarity", 2006
INXS,  "Kick", 1987
Iron Maiden,  "Killers", 1981
Iron Maiden,  "The Number of the Beast", 1982
Iron Maiden,  "Piece of Mind", 1983
Iron Maiden,  "Powerslave", 1984
Iron Maiden,  "Somewhere in Time", 1986
Iron Maiden,  "Seventh Son of a Seventh Son", 1988
Iron Maiden,  "No Prayer for the Dying", 1990
Iron Maiden,  "Fear Of The Dark", 1992
Iron Maiden,  "Brave New World", 2000
Iron Maiden,  "Dance of Death", 2003
Iron Maiden,  "A Matter of Life and Death", 2006
Iron Maiden,  "The Final Frontier", 2010
Iron Maiden,  "The Book Of Souls", 2015
J Mascis,  "Martin + Me", 1996
J Mascis,  "Live at CBGB's: The First Acoustic Show", 2006
Jamiroquai,  "Travelling Without Moving", 1996
Janis Joplin,  "Janis Joplin's Greatest Hits", 1973
Jeff Buckley,  "Grace", 1994
Jeff Buckley,  "Sketches for My Sweetheart the Drunk", 1998
Jimi Hendrix,  "Experience Hendrix: The Best Of Jimi Hendrix", 1998
Joe Satriani,  "The Essential Joe Satriani", 2010
John Lennon,  "Imagine", 1971
John Mellencamp,  "American Fool", 1982
Johnny Cash,  "The Essential Johnny Cash", 2002
Johnny Cash,  "The Box Set Series", 2014
Journey,  "Escape", 1981
Joy Division,  "Substance", 1988
Kavinsky,  "OutRun", 2013
Killswitch Engage,  "As Daylight Dies", 2006
KISS,  "The Very Best Of Kiss", 2002
Korn,  "Korn", 1994
Korn,  "Life Is Peachy", 1996
Korn,  "Follow the Leader", 1998
Korn,  "Issues", 1999
Korn,  "Untouchables", 2002
Korn,  "Take a Look in the Mirror", 2003
Korn,  "Greatest Hits, Volume 1", 2004
Korn,  "See You On The Other Side", 2005
Korn,  "Untitled", 2007
Korn,  "Korn III: Remember Who You Are", 2010
Korn,  "The Path Of Totality", 2011
Korn,  "The Paradigm Shift", 2013
Led Zeppelin,  "Mothership", 2007
Len,  "You Can't Stop the Bum Rush", 1999
Limp Bizkit,  "Three Dollar Bill, Yall$", 1997
Limp Bizkit,  "Significant Other", 1999
Limp Bizkit,  "Chocolate Starfish and the Hot Dog Flavored Water", 2000
Limp Bizkit,  "Hybrid Theory", 2000
Limp Bizkit,  "Results May Vary", 2003
Limp Bizkit,  "Gold Cobra", 2011
Linkin Park,  "Meteora", 2003
Live,  "Throwing Copper", 1994
Live,  "Secret Samadhi", 1997
Live,  "The Distance to Here", 1999
Lou Reed,  "The Very Best Of Lou Reed", 1999
Lou Reed,  "Transformer", 1972
Lou Reed,  "Berlin", 1973
Lou Reed,  "Coney Island Baby", 1975
Lou Reed,  "The Blue Mask", 1982
Lou Reed,  "Legendary Hearts", 1983
Lou Reed,  "New Sensations", 1984
Lou Reed,  "Mistrial", 1986
Lou Reed,  "New York", 1989
Lynyrd Skynyrd,  "Pronounced 'Lĕh-'nérd 'Skin-'nérd", 1973
M.I.A.,  "Kala", 2007
Manic Street Preachers,  "The Holy Bible", 1994
Marcy Playground,  "Marcy Playground", 1997
Marilyn Manson,  "Portrait of an American Family", 1994
Marilyn Manson,  "Antichrist Superstar", 1996
Marilyn Manson,  "Mechanical Animals", 1998
Marilyn Manson,  "Holy Wood (In the Shadow of the Valley of Death)", 2000
Marilyn Manson,  "The Golden Age Of Grotesque", 2003
Marilyn Manson,  "Lest We Forget: The Best Of", 2004
Marilyn Manson,  "Eat Me, Drink Me", 2007
Marilyn Manson,  "The High End Of Low", 2009
Marvin Gaye,  "What's Going On", 1971
Meat Loaf,  "Bat Out of Hell", 1977
Megadeth,  "Rust In Peace", 1990
Megadeth,  "Greatest Hits: Back To The Start", 2005
Megadeth,  "United Abominations", 2007
Melvins,  "Houdini", 1993
Metallica,  "…And Justice For All", 1988
Metallica,  "Metallica", 1991
Metallica,  "Garage, Inc.", 1998
Michael Jackson,  "Number Ones", 2003
Millencolin,  "Pennybridge Pioneers", 2000
Mr. Big,  "Lean Into It", 1991
Muse,  "Black Holes and Revelations", 2006
My Chemical Romance,  "Three Cheers For Sweet Revenge", 2004
My Chemical Romance,  "The Black Parade", 2006
N.W.A,  "Straight Outta Compton", 1988
Nas,  "Illmatic", 1994
Nas,  "Stillmatic", 2001
Naughty By Nature,  "Naughty By Nature", 1991
Naughty By Nature,  "19 Naughty III", 1993
Naughty By Nature,  "Poverty's Paradise", 1995
Neil Young,  "Greatest Hits", 2004
Nena,  "99 Luftballons", 1983
New Order,  "Brotherhood", 1986
New Order,  "Substance 1987", 1987
New Order,  "(The Best of) New Order", 1995
New Radicals,  "Maybe You've Been Brainwashed Too", 1998
Nico Vega,  "Fury Oh Fury EP", 2013
Night Ranger,  "Midnight Madness", 1983
Nightwish,  "Dark Passion Play", 2007
Nine Inch Nails,  "Pretty Hate Machine", 1989
Nine Inch Nails,  "The Downward Spiral", 1994
Nine Inch Nails,  "The Fragile", 1999
Nine Inch Nails,  "With Teeth", 2005
Nirvana,  "Nevermind", 1991
Nirvana,  "Incesticide", 1992
Nirvana,  "In Utero", 1993
Nirvana,  "MTV Unplugged in New York", 1994
Nirvana,  "Nirvana", 2002
No Doubt,  "The Singles 1992-2003", 2003
NOFX,  "Punk in Drublic", 1994
NOFX,  "Coaster", 2009
Orgy,  "Candyass", 1998
Orgy,  "Vapor Transmission", 2000
P.O.D.,  "Satellite", 2001
Papa Roach,  "Infest", 2000
Papa Roach,  "Lovehatetragedy", 2002
Papa Roach,  "Getting Away With Murder", 2004
Papa Roach,  "The Paramour Sessions", 2006
Papa Roach,  "Metamorphosis", 2009
Patti Smith Group,  "Easter", 1978
Pearl Jam,  "Ten", 1991
Pearl Jam,  "Vs.", 1993
Pearl Jam,  "Vitalogy", 1994
Pearl Jam,  "No Code", 1996
Pearl Jam,  "Yield", 1998
Pearl Jam,  "Binaural", 2000
Pearl Jam,  "Riot Act", 2002
Pearl Jam,  "Pearl Jam", 2006
Pearl Jam,  "Backspacer", 2009
Pearl Jam,  "Lightning Bolt", 2013
Pennywise,  "Full Circle", 1997
Pet Shop Boys,  "Discography: The Complete Singles Collection", 1991
Phantom Planet,  "The Guest", 2002
Phil Collins,  "…Hits", 1998
Pixies,  "Doolittle", 1989
Pixies,  "Wave of Mutilation: Best of Pixies", 2004
Placebo,  "Once More With Feeling: Singles 1996–2004", 2004
Powerman 5000,  "Tonight the Stars Revolt!", 1999
Powerman 5000,  "Transform", 2003
Powerman 5000,  "Copies, Clones & Replicants", 2011
Puscifer,  "Don't Shoot the Messenger", 2007
Puscifer,  """"C"" is for (Please Insert Sophomoric Genitalia Reference HERE)"", 2009
Queen,  "A Night At The Opera", 1975
Queen,  "Greatest Hits I", 1981
Queen,  "Greatest Hits II", 1991
Queensryche,  "Operation: Mindcrime", 1988
Queensryche,  "Empire", 1990
R.E.M.,  "Out of Time", 1991
R.E.M.,  "The Best of R.E.M.", 1991
R.E.M.,  "Automatic For The People", 1992
R.E.M.,  "In Time: The Best of R.E.M. 1988 - 2003", 2003
Radiohead,  "The Best Of Radiohead", 2008
Rage Against the Machine,  "Rage Against the Machine", 1992
Rage Against the Machine,  "Evil Empire", 1996
Rage Against the Machine,  "The Battle of Los Angeles", 1999
Rage Against the Machine,  "Renegades", 2000
Rammstein,  "Sehnsucht", 1997
Rammstein,  "Mutter", 2001
Rammstein,  "Reise, Reise", 2004
Rammstein,  "Rosenrot", 2005
Rammstein,  "Liebe Ist Für Alle Da", 2009
Ramones,  "Anthology: Hey Ho, Let’s Go!", 2001
Red Hot Chili Peppers,  "Blood Sugar Sex Magik", 1991
Red Hot Chili Peppers,  "Californication", 1999
Richard Marx,  "Rush Street", 1991
Rilo Kiley,  "More Adventurous", 2004
Rob Zombie,  "Past, Present & Future", 2003
Rush,  "Chronicles", 1990
Santana,  "The Essential Santana", 2002
Sex Pistols,  "Kiss This: The Best Of The Sex Pistols", 1992
Simon & Garfunkel,  "The Essential Simon & Garfunkel", 2003
Simple Minds,  "Glittering Prize 81/92", 1992
Slipknot,  "Slipknot", 1999
Slipknot,  "Iowa", 2001
Slipknot,  "Vol. 3: (The Subliminal Verses)", 2004
Slipknot,  "All Hope Is Gone", 2008
Slipknot,  ".5: The Gray Chapter", 2014
Social Distortion,  "Greatest Hits", 2007
Soul Asylum,  "Grave Dancers Union", 1992
Soul Asylum,  "Let Your Dim Light Shine", 1995
Soundgarden,  "Badmotorfinger", 1991
Soundgarden,  "Superunknown", 1994
Soundgarden,  "Down on the Upside", 1996
Spin Doctors,  "Pocket Full of Kryptonite", 1991
Staind,  "Dysfunction", 1999
Staind,  "Break the Cycle", 2001
Staind,  "14 Shades Of Grey", 2003
Stevie Wonder,  "Innervisions", 1991
Stone Temple Pilots,  "Core", 1992
Stone Temple Pilots,  "Purple", 1994
Stone Temple Pilots,  "Tiny Music...Songs from the Vatican Gift Shop", 1996
Stone Temple Pilots,  "No. 4", 1999
Stone Temple Pilots,  "Shangri-La Dee Da", 2001
Stone Temple Pilots,  "Stone Temple Pilots", 2010
Sublime,  "Sublime", 1996
Sugar Ray,  "Floored", 1997
Sugar Ray,  "14:59", 1999
Sugar Ray,  "Sugar Ray", 2001
Suzanne Vega,  "Solitude Standing", 1987
System of a Down,  "System of a Down", 1998
System of a Down,  "Toxicity", 2001
System of a Down,  "Steal This Album!", 2002
System of a Down,  "Hypnotize", 2005
System of a Down,  "Mezmerize", 2005
Talking Heads,  "The Best Of Talking Heads: Once in a Lifetime", 1992
Tears for Fears,  "Songs From the Big Chair", 1985
The Beach Boys,  "Sounds of Summer: The Very Best of the Beach Boys", 2003
The Bloodhound Gang,  "One Fierce Beer Coaster", 1996
The Bloodhound Gang,  "Hooray for Boobies", 2000
The Cardigans,  "First Band On The Moon", 1996
The Cardigans,  "Gran Turismo", 1998
The Cars,  "Heartbeat City", 1984
The Cars,  "The Cars Greatest Hits", 1985
The Chemical Brothers,  "Dig Your Own Hole", 1997
The Clash,  "London Calling", 1979
The Clash,  "The Singles", 2007
The Cranberries,  "Stars [The Best Of 1992-2002]", 2002
The Crystal Method,  "Legion of Boom", 2004
The Cure,  "Greatest Hits", 2001
The Darkness,  "Permission to Land", 2003
The Distillers,  "Sing Sing Death House", 2002
The Distillers,  "Coral Fang", 2003
The Doors,  "The Very Best Of The Doors", 2007
The Human League,  "Dare!", 1981
The Kinks,  "Kinks", 1964
The Last Internationale,  "We Will Reign", 2014
The Lemonheads,  "It's A Shame About Ray", 1992
The Marvelettes,  "Please Mr. Postman", 1961
The Misfits,  "Static Age", 1996
The Offspring,  "Ignition", 1992
The Offspring,  "Smash", 1994
The Offspring,  "Ixnay On The Hombre", 1997
The Offspring,  "Americana", 1998
The Offspring,  "Conspiracy Of One", 2000
The Offspring,  "Splinter", 2003
The Police,  "The Police", 2007
The Primitives,  "Lovely", 1988
The Proclaimers,  "The Best Of The Proclaimers", 2002
The Prodigy,  "Music For The Jilted Generation", 1994
The Prodigy,  "The Fat of the Land", 1997
The Prodigy,  "Always Outnumbered Never Outgunned", 2004
The Prodigy,  "Invaders Must Die", 2009
The Smashing Pumpkins,  "Gish", 1991
The Smashing Pumpkins,  "Siamese Dream", 1993
The Smashing Pumpkins,  "Pisces Iscariot", 1994
The Smashing Pumpkins,  "Mellon Collie And The Infinite Sadness", 1995
The Smashing Pumpkins,  "Adore", 1998
The Smashing Pumpkins,  "MACHINA/the machines of God", 2000
The Smashing Pumpkins,  "Zeitgeist", 2007
The Smashing Pumpkins,  "Oceania", 2012
The Smiths,  "The Sound of The Smiths", 2008
The Strokes,  "Is This It", 2001
The Strokes,  "Room on Fire", 2003
The Strokes,  "First Impressions of Earth", 2006
The Strokes,  "Angles", 2011
The Velvet Underground,  "The Velvet Underground & Nico", 1967
The Velvet Underground,  "White Light/White Heat", 1968
The Velvet Underground,  "Loaded", 1970
The White Stripes,  "Elephant", 2003
The White Stripes,  "Icky Thump", 2007
The Who,  "Tommy", 1969
The Who,  "Who’s Next", 1971
The Yardbirds,  "The Very Best Of The Yardbirds", 2003
Tool,  "Undertow", 1993
Tool,  "Ænima", 1996
Tool,  "Lateralus", 2001
Tool,  "10,000 Days", 2006
Toto,  "Toto IV", 1982
Trivium,  "The Crusade", 2006
Twisted Sister,  "Stay Hungry", 1984
U2,  "The Best of 1980-1990", 1998
Ugly Kid Joe,  "America's Least Wanted", 1992
Vampire Weekend,  "Vampire Weekend", 2008
Vampire Weekend,  "Contra", 2010
Vampire Weekend,  "Modern Vampires of the City", 2013
Velvet Revolver,  "Contraband", 2004
Violent Femmes,  "Violent Femmes", 1983
Weezer,  "Weezer (Blue Album)", 1994
Weezer,  "Pinkerton", 1996
Weezer,  "Weezer (Green Album)", 2001
Weezer,  "Maladroit", 2003
Weezer,  "Make Believe", 2005
Weezer,  "Weezer (Red Album)", 2008
Weezer,  "Raditude", 2009
Weezer,  "Hurley", 2010
White Zombie,  "Supersexy Swingin' Sounds", 1996
Wilson Pickett,  "The Very Best of Wilson Pickett", 1993
Yeah Yeah Yeahs,  "Yeah Yeah Yeahs", 2002
Yeah Yeah Yeahs,  "Fever To Tell", 2003
Yeah Yeah Yeahs,  "Show Your Bones", 2006
Yeah Yeah Yeahs,  "Is Is", 2007
Yeah Yeah Yeahs,  "It's Blitz!", 2009
Yeah Yeah Yeahs,  "Mosquito", 2013
Zwan,  "Mary Star Of The Sea", 2002
Nas,  "It Was Written", 1996
