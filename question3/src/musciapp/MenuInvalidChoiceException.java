package musciapp;

public class MenuInvalidChoiceException extends Exception {
    public MenuInvalidChoiceException() {
        super();
    }

    public MenuInvalidChoiceException(String message) {
        super(message);
    }

    public MenuInvalidChoiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public MenuInvalidChoiceException(Throwable cause) {
        super(cause);
    }

    protected MenuInvalidChoiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
