package musciapp;

import java.util.*;

/**
 * This the application for running the music app.
 * TODO step b. - step i. modify this class appropriately
 *  @author Write your UPI here
 */
public class MusicApp {
    // This is the main method. Do not change this!
    public static void main(String[] args) {
        new MusicApp().start();
    }

    // This is the start method. Do not change this!
    private void start() {
        System.out.println("Welcome to the 90's Music App.");
        int userChoice = -1;
        while (userChoice != 4) {
            System.out.println("===========================");
            mainMenu();
            userChoice = getUserMenuChoice();
            if (userChoice == 1) {
                System.out.println("Available Genres: Metal, Pop, Rap, Rock");
                String genre = getUserGenreChoice();
                Set<String> albumTitlesByGenre = getAlbumTitlesByGenre(AlbumCollection.ALBUMS, genre);
                System.out.println("-------------------------------------");
                System.out.println("All available " + genre.toLowerCase() + " albums:");
                System.out.println("-------------------");
                System.out.println(albumTitlesByGenre);
            }

            if (userChoice == 2) {
                System.out.println("Please enter any search terms: ");
                Set<Album> albums = findAlbumsByTitle(AlbumCollection.ALBUMS, Keyboard.readInput());
                System.out.println("-------------------------------------");
                if (albums.size() == 0) {
                    System.out.println("No albums found.");
                    continue;
                }
                System.out.println("Album(s) found:");
                System.out.println("-------------------");
                for (Album album : albums) {
                    System.out.println(album.getArtist() + ",  \"" + album.getTitle() + "\", " + album.getGenre() + ", " + album.getYear());
                }
            }

            if (userChoice == 3) {
                Map<String, Integer> artistsAndAlbums = processAlbums(AlbumCollection.ALBUMS);
                System.out.println("-------------------------------------");
                printAllArtists(artistsAndAlbums);
            }
        }
        System.out.println("Good bye! Please visit again!");
    }

    // This method displays the main menu. Do not change this!
    private void mainMenu() {
        System.out.println("1. Available Albums by Genre");
        System.out.println("2. Search Albums by Title");
        System.out.println("3. All Available Artists");
        System.out.println("4. Quit");
    }

    // TODO step b. complete the validateGenre method
    private String validateGenre(String input) throws MusicGenreInvalidException{
        if (input==null){
            throw new MusicGenreInvalidException("Empty Input!");
        } else if(!input.equalsIgnoreCase("Metal") && !input.equalsIgnoreCase("Rap") && !input.equalsIgnoreCase("Rock") && !input.equalsIgnoreCase("Pop")){
            throw new MusicGenreInvalidException("Invalid Genre!");
        }
        return input;
    }

    // TODO step c. modify the getUserGenreChoice method
    private String getUserGenreChoice() {
        while (true) {
            System.out.print("Please enter a genre: ");
            try {
                return validateGenre(Keyboard.readInput());
            } catch (MusicGenreInvalidException e){
                System.out.println(e);
            }
        }
    }

    // TODO step d. complete the validateUserInput method
    private int validateUserInput(String input) throws MenuInvalidChoiceException {
        //check if string empty and throw exception if so
        if(input==null){
            throw new MenuInvalidChoiceException("Empty Input!");
        }

        //check if string can be parsed to integer, if not throw that exception
        int userChoice=0;
        try{
            userChoice = Integer.parseInt(input);
        } catch (NumberFormatException e){
            throw new MenuInvalidChoiceException("Invalid number!");
        }

        //finally if the user choice is not within the acceptable values, you know what we do.
        if (userChoice<1 || userChoice>4){
            throw new MenuInvalidChoiceException("Invalid number!");
        }
        return userChoice;
    }

    // TODO step e. modify the getUserGenreChoice method
    private int getUserMenuChoice() {
        while (true) {
            System.out.print("Please choose a number from the menu: ");
            try {
                return validateUserInput(Keyboard.readInput());
            } catch (MenuInvalidChoiceException e){
                System.out.println(e);
            }
        }
    }

    // TODO step f. complete the getAlbumTitlesByGenre method
    private Set<String> getAlbumTitlesByGenre(Album[] albums, String genre) {
        Set<String> albumsByGenre = new TreeSet();

        for (int i = 0; i < albums.length; i++) {
            if(albums[i].getGenre().equalsIgnoreCase(genre)){
                albumsByGenre.add(albums[i].getGenre());
            }
        }
        return albumsByGenre;
    }

    // TODO step g. complete the findAlbumsByTitle method
    private Set<Album> findAlbumsByTitle(Album[] albums, String query) {
        Set<Album> albumsByTitle = new HashSet<>();

        for (int i = 0; i < albums.length; i++) {
            if(albums[i].getTitle().toLowerCase().contains(query.toLowerCase())){
                albumsByTitle.add(albums[i]);
            }
        }
        return albumsByTitle;
    }

    // TODO step h. complete the processAlbums method
    private Map<String, Integer> processAlbums(Album[] albums) {
        Map<String,Integer> processedAlbums = new TreeMap<>();

        for (int i = 0; i < albums.length; i++) {
            int countArtist=0;
            for (int j = 0; j < albums.length; j++) {
                String artist = albums[i].getArtist();
                if(albums[j].getArtist().equalsIgnoreCase(artist)){
                    countArtist++;
                }
            }
            processedAlbums.put(albums[i].getArtist(),countArtist);
        }

        return processedAlbums;
    }

    // TODO step i. complete the printAllArtists method
    private void printAllArtists(Map<String, Integer> artistsAndAlbums) {


//
//        artistsAndAlbums.entrySet().forEach((k,v)->System.out.println(k,v));
//        for ((k,v)->System.out.println()) {
//
//        }
        //not finished this last
        for (String key:artistsAndAlbums.keySet()
             ) {
//            String[] artists =
        }

//        System.out.printf("",artistsAndAlbums.keySet(),);
    }

}
