package musciapp;

public class MusicGenreInvalidException extends Exception {
    public MusicGenreInvalidException() {
        super();
    }

    public MusicGenreInvalidException(String message) {
        super(message);
    }

    public MusicGenreInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public MusicGenreInvalidException(Throwable cause) {
        super(cause);
    }

    protected MusicGenreInvalidException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
