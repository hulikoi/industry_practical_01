package musicstore;

/**
 * This class is used for storing each album entry.
 *
 * @author Yu-Cheng Tu
 */
public class Album implements Comparable {
    private int id;
    private Genre genre;
    private String artist;
    private String title;
    private int year;
    private int tracks;
    private int discs;

    public Album(int id, String title, Genre genre, String artist, int year, int tracks, int discs) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.artist = artist;
        this.year = year;
        this.tracks = tracks;
        this.discs = discs;
    }

    public int getId() {
        return id;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public int getTracks() {
        return tracks;
    }

    public int getDiscs() {
        return discs;
    }

    @Override
    public String toString() {
        return String.format("%s,  \"%s\", %d", artist, title, year);
    }

    //adding compareTo override, setting so larger numbers get returned first.
    @Override
    public int compareTo(Object o) {
        if (o instanceof Album) {
            Album otherAlbum = (Album) o;

            double difference = this.getYear() - otherAlbum.getYear();
            if (difference > 0) return -1;
            else if (difference < 0) return 1;
            else return 0;
        }
        return 0;
    }
}
