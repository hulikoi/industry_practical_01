package musicstore;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.List;

/**
 * This the application for running the music store.
 * TODO step b. - step f. modify this class appropriately
 *
 * @author Write your UPI here
 */
public class MusicStore {

    // This is the main method. Do not change this!
    public static void main(String[] args) {
        new MusicStore().start();
    }

    // This is the start method. Do not change this!
    private void start() {
        System.out.println("Welcome to the Auckland ICT Music Store");
        System.out.println("===============================");

        List<Album> albums = getAlbums("albums.csv");

        System.out.println("We have a total number of " + albums.size() + " albums available in store.");
        System.out.println("--------------------------------------------");
        System.out.println("Here is a quick summary of what we have in store for you :)");
        System.out.println("--------------------------------------------");

        System.out.println("Country albums: " + getTotalByGenre(albums, Genre.COUNTRY));
        System.out.println("Hip hop/rap albums: " + getTotalByGenre(albums, Genre.HIPHOP));
        System.out.println("Metal albums: " + getTotalByGenre(albums, Genre.METAL));
        System.out.println("Pop albums: " + getTotalByGenre(albums, Genre.POP));
        System.out.println("Rock albums: " + getTotalByGenre(albums, Genre.ROCK));
        System.out.println("Other albums: " + getTotalByGenre(albums, Genre.OTHER));

        System.out.println("--------------------------------------------");
        System.out.println("Exporting album information to a file was " +
                (exportAlbumsToFile(albums, "albumSummary.txt") ? "successful!" : "not successful :("));

        System.out.println("--------------------------------------------");
        System.out.println("Latest 10 albums in store");
        System.out.println("--------------------------------------------");

        printLatestTenAlbums(albums);

        System.out.println("--------------------------------------------");
    }

    /**
     * Creates an album based on the given information.
     * Do not modify this method!
     *
     * @param albumInfo String array containing information about an album1
     * @return an album object
     */
    private Album createAlbum(String[] albumInfo) {
        int id = Integer.parseInt(albumInfo[0]);
        String album = albumInfo[1];
        Genre genre = getGenre(albumInfo[2]);
        String artist = albumInfo[3];
        int year = Integer.parseInt(albumInfo[4]);
        int tracks = Integer.parseInt(albumInfo[5]);
        int discs = Integer.parseInt(albumInfo[6]);
        return new Album(id, album, genre, artist, year, tracks, discs);
    }

    // TODO step b. complete the getGenre method
    private Genre getGenre(String genre) {
        //using switch to return Genre enum
        switch (genre) {
            case "Country":
                return Genre.COUNTRY;
            case "Hip Hop/Rap":
                return Genre.HIPHOP;
            case "Metal":
                return Genre.METAL;
            case "Pop":
                return Genre.POP;
            case "Rock":
                return Genre.ROCK;
            default:
                return Genre.OTHER;
        }
    }

    // TODO step c. complete the getAlbums method
    private List<Album> getAlbums(String filePath) {
        List<Album> albumList = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))) {
            String line;
            // disposing first line of the file.
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] albumArray = line.split(";");
                Album album = createAlbum(albumArray);
                albumList.add(album);
            }

        } catch (FileNotFoundException e) {
            System.out.print(e);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.print(e);
            e.printStackTrace();
        } catch (Exception e) {
            System.out.print(e);
            e.printStackTrace();
        }
        return albumList;
    }

    // TODO step d. complete the getTotalByGenre method
    private int getTotalByGenre(List<Album> albums, Genre genre) {
        int genreNum = 0;

        for (int i = 0; i < albums.size(); i++) {
            if (albums.get(i).getGenre().equals(genre)) {
                genreNum++;
            }
        }
        return genreNum;
    }

    // TODO step e. complete the exportAlbumsToFile method
    private boolean exportAlbumsToFile(List<Album> albums, String filePath) {

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filePath)))) {
            for (int i = 0; i < albums.size(); i++) {
                //get the toString of each album, write to file w/buffered writer, add new line after. rinse and repeat.
                String albumInfo = albums.get(i).toString();

                bw.write(albumInfo);
                bw.newLine();
            }
        } catch (Exception e) {
            //catching any exception and returning false
            return false;
        }
        return true;
    }

    // TODO step f. complete printLatestTenAlbums method
    private void printLatestTenAlbums(List<Album> albums) {
        //sorting collection
        albums.sort(Album::compareTo);
        //printing output
        for (int i = 0; i < 10; i++) {
            System.out.println(albums.get(i));
        }

    }
}
