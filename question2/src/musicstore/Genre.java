package musicstore;

public enum Genre {
    COUNTRY, HIPHOP, METAL, POP, ROCK, OTHER
}
